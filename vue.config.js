const path = require('path')
var webpack = require('webpack')
const publicDir = 'assets'

function publicPath () {
  if (process.env.CI_PAGES_URL) {
    return new URL(process.env.CI_PAGES_URL).pathname
  } else {
    return '/'
  }
}

module.exports = {
  publicPath: publicPath(),

  outputDir: 'public',

  transpileDependencies: [
    'vuetify', '@koumoul/vjsf'
  ],

  configureWebpack: {
    plugins: [
      new webpack.EnvironmentPlugin(['CLIENT_ID', 'GITLAB_URL'])
    ]
  },

  chainWebpack (config) {
    config.plugin('html').tap((args) => {
      args[0].template = path.resolve(publicDir, 'index.html')
      return args
    })
    config
      .plugin('copy')
      .use(require('copy-webpack-plugin'))
      .tap((args) => {
        return [
          [...(args[0] ? args[0] : []), { from: path.resolve(publicDir) }]
        ]
      })
  }
}
